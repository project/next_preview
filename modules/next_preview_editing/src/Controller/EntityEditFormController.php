<?php

namespace Drupal\next_preview_editing\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\EntityInterface;

/**
 * Defines EntityEditFormController controller.
 */
class EntityEditFormController extends ControllerBase {

  /**
   * Render the entity edit form.
   *
   * @return array
   *   Return markup array.
   *
   * @throws \Drupal\Core\Entity\EntityStorageException
   */
  public function renderForm(EntityInterface $entity) {
    /** @var \Drupal\Core\Entity\EntityFormBuilder $form_builder */
    $form_builder = \Drupal::service('entity.form_builder');

    $form = $form_builder->getForm($entity, 'edit', []);

    unset($form['content_translation']);
    unset($form['actions']['delete_translation']);

    $form['#attached']['library'][] = 'next_preview_editing/next_preview_editing.form';

    return $form;
  }

}
