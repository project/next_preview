<?php

/**
 * Alter the frontend URL and API path for page preview.
 *
 * @param $frontend
 *   The frontend URL.
 * @param $api_path
 *   The NextJS API path.
 * @param $path
 *   The current path.
 *
 * @return void
 */
function hook_next_preview_frontend_alter(&$frontend, &$api_path, $path) {
  $frontend = 'http://localhost:3000';
  $api_path = '/api/page-preview';
}

