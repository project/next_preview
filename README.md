# NextJS Headless Preview

## Description

This module provides a preview iFrame of your NextJS application on the node view page in Drupal, 
including a button to open front-end in preview mode.

#### NextJS Headless Preview: Editing

This submodule provides an isolated edit form that can be iFramed to allow editing in the front-end application.


## Requirements

You need the NPM package [`next-drupal-preview`](https://www.npmjs.com/package/next-drupal-preview) installed and set up in your NextJS application. Please refer to the [documentation](https://www.npmjs.com/package/next-drupal-preview).


## Installation

The installation of this module is like other Drupal modules.

1. If your site is [managed via Composer](https://www.drupal.org/node/2718229),
   use Composer to download the next_preview module running

   ```composer require "drupal/next_preview"```. 

   Otherwise, copy/upload the next_preview module to the modules directory of your Drupal installation.

2. Enable the 'NextJS Headless Preview' module and desired sub-modules in 'Extend'.
   (`/admin/modules`)

3. Set up user permissions. (`/admin/people/permissions`)

## Configuration
Please refer to the [module's project page](https://www.drupal.org/project/next_preview).


## Front-end integration

Please refer to the next-drupal-preview [documentation](https://www.npmjs.com/package/next-drupal-preview) for instructions on how to install the NPM module and integrate it in your front-end application.


## Configuration

- The configuration form can be found under "Configuration > Content authoring". (`/admin/config/content/preview`)
- Make sure your front-end application allows being embedded in an iframe from your Drupal site.


## Maintainers

Current maintainers for Drupal 8/9:

- [Perry Janssen](https://www.drupal.org/u/perryjanssen) for organisation [iO](https://www.drupal.org/node/2242363)
