<?php

namespace Drupal\next_preview\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Page preview settings.
 */
class NextPreviewConfigForm extends ConfigFormBase {

  /**
   * Config name.
   */
  const CONFIG = 'next_preview.settings';

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'next_preview_config_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
   return [self::CONFIG];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $config = $this->config(self::CONFIG);

    $form['fe_url'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Frontend URL'),
      '#description' => $this->t('URL of the frontend application. <pre>Example: https://example.com</pre>'),
      '#default_value' => $config->get('fe_url'),
    ];

    $form['api_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Preview endpoint'),
      '#description' => $this->t('Path of the NextJS preview endpoint, starting with a /. <pre>Example: /api/preview</pre>'),
      '#default_value' => $config->get('api_path'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $config = $this->configFactory->getEditable(self::CONFIG);

    $config->set('fe_url', $form_state->getValue('fe_url'));
    $config->set('api_path', $form_state->getValue('api_path'));

    $config->save();
    parent::submitForm($form, $form_state);
  }

}
